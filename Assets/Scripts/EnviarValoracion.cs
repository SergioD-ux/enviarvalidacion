﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class EnviarValoracion : MonoBehaviour
{

    public GameObject nombre;
    public GameObject valoracion;
    public GameObject mensaje;

    private string Nombre;
    private string Valoracion;
    private string Mensaje;

  
    IEnumerator Post(string nombre, string valoracion, string mensaje)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.1884265043", nombre);
        form.AddField("entry.1943660899", valoracion);
        form.AddField("entry.1596041665", mensaje);
        byte[] rawData = form.data;
        UnityWebRequest www = UnityWebRequest.Post("https://docs.google.com/forms/u/0/d/e/1FAIpQLSdPc9EJSywA2t9z-qY7IBJjdd34Umoz-63XsSd3v8atlah1ow/formResponse", form);
        yield return www.SendWebRequest();
    }

    public void Enviar()
    {
        Nombre = nombre.GetComponent<InputField>().text;
        Valoracion = valoracion.GetComponent<InputField>().text;
        Mensaje = mensaje.GetComponent<InputField>().text;
        StartCoroutine(Post(Nombre, Valoracion, Mensaje));

    }
}
