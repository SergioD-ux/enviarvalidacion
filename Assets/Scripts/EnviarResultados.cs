﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using UnityEngine.Networking;

public class Resultados : MonoBehaviour
{
    public List<Text> txtNombre, txtIngreso, txtIngresoExtra, txtBanco, txtSocio, txtUtilidad;
    public List<Image> imgIconos;

    private void Start()
    {
        DarResultados();
    }

    public void DarResultados()
    {
        OperarData();
        OrdenarPuestos();

        for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
        {
            txtNombre[i].text = ValidacionCampos.listaJugadores[i].Nombre;
            txtNombre[i].gameObject.SetActive(true);

            txtIngreso[i].text = ValidacionCampos.listaJugadores[i].DineroActual.ToString();
            txtIngreso[i].gameObject.SetActive(true);

            float cantMateria1 = ValidacionCampos.listaJugadores[i].CantidadMaterial1;
            float cantMateria2 = ValidacionCampos.listaJugadores[i].CantidadMaterial2;
            float cantMateria3 = ValidacionCampos.listaJugadores[i].CantidadMaterial3;
            float cantMateria4 = ValidacionCampos.listaJugadores[i].CantidadMaterial4;

            txtIngresoExtra[i].text = ((cantMateria1 + cantMateria2 + cantMateria3 + cantMateria4) * 30).ToString();
            txtIngresoExtra[i].gameObject.SetActive(true);

            txtBanco[i].text = ValidacionCampos.listaJugadores[i].PrestamoBancarioDevolucionTotal.ToString();
            txtBanco[i].gameObject.SetActive(true);

            txtSocio[i].text = ValidacionCampos.listaJugadores[i].PrestamoSocioDevolucionTotal.ToString();
            txtSocio[i].gameObject.SetActive(true);

            txtUtilidad[i].text = ValidacionCampos.listaJugadores[i].DineroGanado.ToString();
            txtUtilidad[i].gameObject.SetActive(true);

            imgIconos[i].gameObject.SetActive(true);

        }
        EnviarEstadistica();

    }
    private void EnviarEstadistica()
    {
        string nro_jugadores = "";
        string nombre_j1 = "";
        string nombre_j2 = "";
        string nombre_j3 = "";
        string nombre_j4 = "";
        string ingresos_j1 = "";
        string ingresos_j2 = "";
        string ingresos_j3 = "";
        string ingresos_j4 = "";
        string utilidades_j1 = "";
        string utilidades_j2 = "";
        string utilidades_j3 = "";
        string utilidades_j4 = "";
        string cant_prestBancario_j1 = "";
        string cant_prestBancario_j2 = "";
        string cant_prestBancario_j3 = "";
        string cant_prestBancario_j4 = "";
        string cant_prestSocio_j1 = "";
        string cant_prestSocio_j2 = "";
        string cant_prestSocio_j3 = "";
        string cant_prestSocio_j4 = "";
        string dinero_prestBancario_j1 = "";
        string dinero_prestBancario_j2 = "";
        string dinero_prestBancario_j3 = "";
        string dinero_prestBancario_j4 = "";
        string dinero_prestSocio_j1 = "";
        string dinero_prestSocio_j2 = "";
        string dinero_prestSocio_j3 = "";
        string dinero_prestSocio_j4 = "";
        string ordenPuesto_j1 = "";
        string ordenPuesto_j2 = "";
        string ordenPuesto_j3 = "";
        string ordenPuesto_j4 = "";
        string cantFinalLeche_j1 = "";
        string cantFinalLeche_j2 = "";
        string cantFinalLeche_j3 = "";
        string cantFinalLeche_j4 = "";
        string cantFinalHuevo_j1 = "";
        string cantFinalHuevo_j2 = "";
        string cantFinalHuevo_j3 = "";
        string cantFinalHuevo_j4 = "";
        string cantFinalFruta_j1 = "";
        string cantFinalFruta_j2 = "";
        string cantFinalFruta_j3 = "";
        string cantFinalFruta_j4 = "";
        string cantFinalHarina_j1 = "";
        string cantFinalHarina_j2 = "";
        string cantFinalHarina_j3 = "";
        string cantFinalHarina_j4 = "";
        string cantProductosSobrantes_j1 = "";
        string cantProductosSobrantes_j2 = "";
        string cantProductosSobrantes_j3 = "";
        string cantProductosSobrantes_j4 = "";

        nro_jugadores = ValidacionCampos.listaJugadores.Count.ToString();
        for (int i = 0; i < ValidacionCampos.listaJugadores.Count; i++)
        {
            nombres += ValidacionCampos.listaJugadores[i].Nombre + " / ";
            ingresos += ValidacionCampos.listaJugadores[i].DineroActual.ToString() + " / ";
            utilidades += ValidacionCampos.listaJugadores[i].DineroGanado.ToString() + " / ";
            cant_prestBancario += ValidacionCampos.listaJugadores[i].NumeroTarjetasPrestamoBanco.ToString() + " / ";
            cant_prestSocio += ValidacionCampos.listaJugadores[i].NumeroTarjetasPrestamoSocio.ToString() + " / ";
            dinero_prestBancario += ValidacionCampos.listaJugadores[i].PrestamoBancario.ToString() + " / ";
            dinero_prestSocio += ValidacionCampos.listaJugadores[i].PrestamoSocio.ToString() + " / ";
            ordenPuesto += ValidacionCampos.listaJugadores[i].OrdenPuesto.ToString() + " / ";
            cantFinalLeche += ValidacionCampos.listaJugadores[i].CantidadMaterial1.ToString() + " / ";
            cantFinalHuevo += ValidacionCampos.listaJugadores[i].CantidadMaterial2.ToString() + " / ";
            cantFinalFruta += ValidacionCampos.listaJugadores[i].CantidadMaterial3.ToString() + " / ";
            cantFinalHarina += ValidacionCampos.listaJugadores[i].CantidadMaterial4.ToString() + " / ";
            cantProductosSobrantes += ValidacionCampos.listaJugadores[i].Cartas.Count.ToString() + " / ";
        }

        StartCoroutine(PostFormEstadisticas(nro_jugadores, nombre_j1, nombre_j2, nombre_j3, nombre_j4, ingresos_j1, ingresos_j2, ingresos_j3, ingresos_j4, utilidades_j1, utilidades_j2, utilidades_j3, utilidades_j4, cant_prestBancario_j1, cant_prestBancario_j2, cant_prestBancario_j3, cant_prestBancario_j3, cant_prestSocio_j1, cant_prestSocio_j2, cant_prestSocio_j3, cant_prestSocio_j4, dinero_prestBancario_j1, dinero_prestBancario_j2, dinero_prestBancario_j3, dinero_prestBancario_j4, dinero_prestSocio_j1, dinero_prestSocio_j2, dinero_prestSocio_j3, dinero_prestSocio_j4, ordenPuesto_j1, ordenPuesto_j2, ordenPuesto_j3, ordenPuesto_j4, cantFinalLeche_j1, cantFinalLeche_j2, cantFinalLeche_j3, cantFinalLeche_j4, cantFinalHuevo_j1, cantFinalHuevo_j2, cantFinalHuevo_j3, cantFinalHuevo_j4, cantFinalFruta_j1, cantFinalFruta_j2, cantFinalFruta_j3, cantFinalFruta_j4, cantFinalHarina_j1, cantFinalHarina_j2, cantFinalHarina_j3, cantFinalHarina_j4, cantProductosSobrantes_j1, cantProductosSobrantes_j2, cantProductosSobrantes_j3, cantProductosSobrantes_j4));

    }

    IEnumerator PostFormEstadisticas(string nro_jugadores, string nombre_j1, string nombre_j2, string nombre_j3, string nombre_j4, string ingresos_j1, string ingresos_j2, string ingresos_j3, string ingresos_j4, string utilidades_j1, string utilidades_j2, string utilidades_j3, string utilidades_j4, string cant_prestBancario_j1, string cant_prestBancario_j2, string cant_prestBancario_j3, string cant_prestBancario_j4, string cant_prestSocio_j1, string cant_prestSocio_j2, string cant_prestSocio_j3, string cant_prestSocio_j4, string dinero_prestBancario_j1, string dinero_prestBancario_j2, string dinero_prestBancario_j3, string dinero_prestBancario_j4, string dinero_prestSocio_j1, string dinero_prestSocio_j2, string dinero_prestSocio_j3, string dinero_prestSocio_j4, string ordenPuesto_j1, string ordenPuesto_j2, string ordenPuesto_j3, string ordenPuesto_j4, string cantFinalLeche_j1, string cantFinalLeche_j2, string cantFinalLeche_j3, string cantFinalLeche_j4, string cantFinalHuevo_j1, string cantFinalHuevo_j2, string cantFinalHuevo_j3, string cantFinalHuevo_j4, string cantFinalFruta_j1, string cantFinalFruta_j2, string cantFinalFruta_j3, string cantFinalFruta_j4, string cantFinalHarina_j1, string cantFinalHarina_j2, string cantFinalHarina_j3, string cantFinalHarina_j4, string cantProductosSobrantes_j1, string cantProductosSobrantes_j2, string cantProductosSobrantes_j3, string cantProductosSobrantes_j4)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.1719570252", nro_jugadores);
        form.AddField("entry.57308637", nombre_j1);
        form.AddField("entry.727868043", nombre_j2);
        form.AddField("entry.1475925457", nombre_j3);
        form.AddField("entry.848941134", nombre_j4);
        form.AddField("entry.1366304157", ingresos_j1);
        form.AddField("entry.1598610505", ingresos_j2);
        form.AddField("entry.954383865", ingresos_j3);
        form.AddField("entry.2034518443", ingresos_j4);
        form.AddField("entry.1279670419", utilidades_j1);
        form.AddField("entry.413782008", utilidades_j2);
        form.AddField("entry.1210173814", utilidades_j3);
        form.AddField("entry.1357082509", utilidades_j4);
        form.AddField("entry.734650411", cant_prestBancario_j1);
        form.AddField("entry.1170906411", cant_prestBancario_j2);
        form.AddField("entry.1385941716", cant_prestBancario_j3);
        form.AddField("entry.1113001756", cant_prestBancario_j4);
        form.AddField("entry.60183126", cant_prestSocio_j1);
        form.AddField("entry.2068863106", cant_prestSocio_j2);
        form.AddField("entry.1470764766", cant_prestSocio_j3);
        form.AddField("entry.371001992", cant_prestSocio_j4);
        form.AddField("entry.915978702", dinero_prestBancario_j1);
        form.AddField("entry.1728707330", dinero_prestBancario_j2);
        form.AddField("entry.988385318", dinero_prestBancario_j3);
        form.AddField("entry.1490783944", dinero_prestBancario_j4);
        form.AddField("entry.789931793", dinero_prestSocio_j1);
        form.AddField("entry.1833149566", dinero_prestSocio_j2);
        form.AddField("entry.1601116181", dinero_prestSocio_j3);
        form.AddField("entry.1141668405", dinero_prestSocio_j4);
        form.AddField("entry.309063750", ordenPuesto_j1);
        form.AddField("entry.1026140618", ordenPuesto_j2);
        form.AddField("entry.1147314285", ordenPuesto_j3);
        form.AddField("entry.13491148", ordenPuesto_j4);
        form.AddField("entry.1262582295", cantFinalLeche_j1);
        form.AddField("entry.1323578504", cantFinalLeche_j2);
        form.AddField("entry.2145933470", cantFinalLeche_j3);
        form.AddField("entry.28856832", cantFinalLeche_j4);
        form.AddField("entry.2016678296", cantFinalHuevo_j1);
        form.AddField("entry.1559475994", cantFinalHuevo_j2);
        form.AddField("entry.910912664", cantFinalHuevo_j3);
        form.AddField("entry.1447142782", cantFinalHuevo_j4);
        form.AddField("entry.1977381554", cantFinalFruta_j1);
        form.AddField("entry.1486891586", cantFinalFruta_j2);
        form.AddField("entry.464202148", cantFinalFruta_j3);
        form.AddField("entry.1567185187", cantFinalFruta_j4);
        form.AddField("entry.1950268055", cantFinalHarina_j1);
        form.AddField("entry.1153332089", cantFinalHarina_j2);
        form.AddField("entry.267903186", cantFinalHarina_j3);
        form.AddField("entry.330564165", cantFinalHarina_j4);
        form.AddField("entry.1991637207", cantProductosSobrantes_j1);
        form.AddField("entry.1773266581", cantProductosSobrantes_j2);
        form.AddField("entry.140533545", cantProductosSobrantes_j3);
        form.AddField("entry.489318687", cantProductosSobrantes_j4);
        byte[] rawData = form.data;
        UnityWebRequest www = UnityWebRequest.Post("https://docs.google.com/forms/u/1/d/e/1FAIpQLSeHNc1eHWkbcM_OJ4EefNcGGNHepoRA_f35fpx1iubDylLv9Q/formResponse", form);
        yield return www.SendWebRequest();
    }
    

}
